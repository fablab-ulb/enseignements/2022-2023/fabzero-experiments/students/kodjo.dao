## 1. Avant-propos
Bienvenu sur ma page fablabzero éxperimentale. Je m'appelle DAO Kodjo Serge. J'ai 21 ans.

![ That's me](images/photo_index.jpg )

## Qui suis-je ?
Je suis né à Lomé, la capitale du Togo, où j'ai vécu jusqu'à l'âge de 12 ans avant de déménager en Belgique. Les rêves plein la tête, dont le plus grand est de créer des méthodes et des objets pour faire avancer le monde scientifique. Pour réaliser ce rêve, j'ai décidé de poursuivre des études en bioingénierie. Étant passionné par les sciences en général, mais avec une préférence pour la chimie, la biologie et en particulier la biotechnologie, la bioingénierie m'a toujours semblé être le meilleur choix car elle combine les techniques et la science. C'est le chemin parfait pour réaliser mes rêves. Cette année, j'entame ma troisième année de bachelier en bioingénierie, et j'ai choisi ce cours comme option. J'avais d'autres options en tête avant de lire la description de ce cours, mais dès que je l'ai lue, j'ai su que c'était celui que je voulais choisir.
Ma devise en matière d'études, c'est que trop d'une matière tue la matière. En d'autres termes, suivre une dizaine de cours de biologie par an ne fera pas de moi un biologiste, mais me dégoûtera de la biologie. En revanche, apprendre un peu de biologie, un peu de chimie et un cours d'ingénierie, c'est amusant. L'un des avantages d'être en bioingénierie est que les cours sont très variés.


## Mes passion dans la vie.

Je suis un grand passionné de football. Je n'ai jamais eu la chance de jouer dans un club, mais je ne rate pas l'occasion d'aller jouer avec mes amis dès que possible. J'aime également regarder le football à la télévision. Je ne vous cache pas que la Coupe du Monde, qui tombe en plein mois de décembre, à la veille des examens, était un gros calvaire pour moi. Mon club préféré est le Barça. Et oui, malgré les défaites en Ligue des Champions chaque année, je suis toujours prêt à crier "Visca El Barça" à tous les matchs.
En plus des matchs, j'aime également faire des recherches. J'aime apprendre sur de nombreux sujets différents : les différentes sciences (biologie, chimie, astronomie, géographie, histoire), le droit, la religion et toutes sortes de choses qui me permettent d'accumuler des connaissances. Les petits quiz sur les réseaux sociaux sont un vrai plaisir pour moi. Toujours très curieux et avide d'apprendre, un ami m'a récemment fait remarquer que je suis une personne épistémophile. Je n'y avais jamais pensé, mais la définition correspond plutôt bien.


##   Mes precédent projet.

Étant en troisième année de bioingénierie, je devais réaliser un projet cette année dans le cadre du cours de gestion de projet et de projet de recherche. J'ai choisi un projet nommé "Ruche et Éléphant". Le projet consiste à résoudre un conflit Homme-faune dans le complexe écologique de PONASI (PO-NAzinga-SIssili) au Burkina Faso.

#### Voici un petit contexte.


Le Burkina, très riche en biodiversité faunique, possède l'une des plus grandes populations d'éléphants de savane en Afrique de l'Ouest. En effet, dans le complexe écologique de PONASI, situé au sud, on dénombre environ 600 têtes d'éléphant.

Ce complexe est un regroupement de trois aires protégées : le parc national Kabore-Timbi ou parc de PO, le Ranch de Nazinga et le complexe de Sissili, reliés par deux couloirs de migration nommés Corridor N°1 et Corridor N°2.

Le premier corridor est au cœur de notre projet. Le Corridor N°1, d'une surface d'environ 4500 hectares de savane arborée, relie le Parc de PO et le Ranch de Nazinga.

Ce corridor, situé dans deux communes (la commune de PO et Guiaro), est entouré de six villages (Bourou, Yaro, Saro, Kollo, Tiakané, Oualem). Si l'implantation de ce corridor représentait déjà un sacrifice en terre agricole pour cette population locale, pendant les saisons humides, saison de migration des éléphants, les champs de ces villages sont victimes d'intrusions et de dégâts considérables. Cela entraîne une insécurité alimentaire et physique, ainsi qu'une perte économique importante pour ces villages, et génère un conflit homme-faune dans la région, plus particulièrement un conflit homme-éléphant.

Impuissante face au pachyderme, la population locale utilise des méthodes peu efficaces qui ne remédient pas au problème.

Avec l'aide de l'ULB-Coopération, une ONG de l'ULB qui possède un bureau au Burkina et dont l'objectif est d'améliorer la vie économique, sociale et environnementale des locaux, notre projet contribue à cet objectif.

Concrètement, les objectifs de notre projet cette année sont de trouver comment résoudre ou diminuer ce conflit homme-éléphant autour de ce corridor, plus précisément comment protéger les champs des villageois contre les intrusions des éléphants et comment les abeilles peuvent aider à éloigner les éléphants ? Nous proposons finalement un protocole basé sur l'utilisation de ruches d'abeilles pour éviter l'intrusion des éléphants.
Pourquoi les abeilles ? Premièrement, parce que les abeilles sont la bête noire des éléphants. Entre ces animaux, c'est une histoire de David contre Goliath, le minuscule contre le géant. Et comme toujours, c'est le minuscule qui gagne. De manière plus scientifique, les éléphants ont très peur des abeilles car bien qu'ils aient une peau épaisse, leur trompe, leurs oreilles et leurs yeux sont très sensibles aux piqûres d'abeilles. Ainsi, si un éléphant se fait piquer une fois dans sa vie, la douleur est si intense qu'il s'en souvient pour toujours. De plus, il évite la zone dès qu'il voit une abeille. C'est sur cette relation tumultueuse entre ces deux protagonistes que notre protocole se base.

La deuxième raison est que, contrairement à d'autres méthodes qui peuvent être coûteuses et ne rien rapporter, la méthode des ruches permet le développement de l'apiculture dans la zone et apporte une source de revenus aux populations locales. De plus, cette méthode respecte l'environnement et les éléphants, en ne leur causant pas de grands dommages, mais en utilisant plutôt leur instinct naturel pour protéger les cultures des villageois.

Pour atteindre nos objectifs, mes collègues et moi avons réalisé un travail remarquable et avons proposé le protocole "WobGo" (qui signifie "éléphant" en mooré, la langue la plus parlée au Burkina). Il s'agit d'une barrière de ruches constituée de ruches actives et de ruches factices interconnectées entre elles par des fils. Elles sont installées de telle manière que si un éléphant bouscule l'un des fils, plusieurs ruches sont perturbées et les abeilles sortent pour aller piquer les éléphants. Ainsi, les abeilles jouent un rôle de gardiennes pour les champs.

Étant donné que j'ai terminé cette documentation avant de recevoir la note et les retours sur le projet, je ne sais pas si le projet a réussi ou pas, mais je croise vraiment les doigts pour que ce protocole soit efficace.

je te glisse également notre rapport ici, si tu veux en savoire plus: [Projet ruche et éléphant](images/RAPPORT%20FINAL%20PROJET%20ELEPHANT%20(1).pdf) 

