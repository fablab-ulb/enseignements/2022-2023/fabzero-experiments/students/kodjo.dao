# 5. Dynamique de groupe et projet final

Ce module est le dernier de la série ( des documentations personnelles). Il est consacré au choix de notre problématique pour les projets finaux, ainsi qu'à toutes les analyses et formations que nous avons effectuées avant de décider de nos projets.

### 1. Cours 1: Analyse et conception de projet

Pour ce premier cours sur la dynamique de groupe, le professeur nous a montré une vidéo sur l'analyse de problématique et de solution. Il s'agit de deux arbres : le premier, l'arbre à problème, est un arbre dont les racines sont les causes de la problématique et les branches, les conséquences de cette problématique. Le deuxième arbre, l'arbre à objectif, est composé de solutions potentielles pour résoudre le problème identifié.

Le but est de nous permettre d'analyser rapidement les problématiques et de chercher des solutions qui peuvent contribuer à régler le problème, car souvent nous nous lançons dans la résolution de problèmes sans vraiment chercher le véritable problème et finissons par trouver des solutions à des problèmes qui ne sont pas réels.
 
Pour adopter cette méthode, le professeur nous a donné comme devoir de choisir une problématique qui nous tient à cœur et de réaliser l'arbre à problématique. [Sanou Kamel](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/kamel.sanou/), [Sami El Hamdou](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/sami.el.hamdou/) et moi  avons décidé de réaliser cet exercice sur la problématique de la surpopulation.


![ Arbre a problematique ](images/Arbre_%C3%A0_probleme.png)

Le sujet est un peu complexe, à première vue c'est ce que je me suis dit aussi, mais la population mondiale vient de dépasser les 8 milliards d'habitants, la question mérite d'être posée : est-ce que la surpopulation est vraiment le problème ou est-ce plutôt les mauvaises actions de cette population qui sont problématiques ? Convaincu par mes amis, j'ai décidé de mettre cet arbre à problématique à l'épreuve et de nous permettre de trouver des problématiques et des solutions.

Effectivement, en commençant par chercher les causes rapidement, je me suis rendu compte que même si nos mauvaises actions ont aggravé la situation, on ne peut pas nier les problèmes liés à la surpopulation. Prenons par exemple les pauvres qui ont beaucoup d'enfants. Je suis sûr que vous en avez déjà entendu parler. Dans de nombreuses populations sous-développées où la pauvreté est très présente, le taux de natalité et le nombre de personnes par famille sont élevés. Et si au contraire, nous prenons les pays très développés, la croissance démographique de ces pays augmente également, mais au lieu d'avoir un taux de natalité élevé, ils ont plutôt un taux de mortalité faible.

La pression démographique qui augmente contribue à l'élargissement des besoins d'un maximum de personnes, accroît la destruction de la biodiversité et de l'habitat des espèces fauniques. Cette conclusion peut sembler tirée par les cheveux, mais je vous laisse effectuer vos propres recherches.

![ Arbre à objectif](images/Arbre_%C3%A0_solution.png)

En ce qui concerne les solutions, évidemment, nous avons pensé au plan Thanos. Un claquement de doigts et tout est réglé. Mais ce n'est jamais aussi simple.

Plus sérieusement, nous avons réfléchi à des actions qui ont déjà été réalisées dans le passé ou qui pourraient être réalisées, aussi folles que ces idées puissent paraître. Par exemple, des taxes sur le nombre d'enfants. Après tout, dans le passé, l'allocation familiale avait pour but de permettre aux gens d'avoir beaucoup d'enfants. Pourquoi ne pas faire l'inverse ?

Mais d'autres solutions comme l'incitation des pauvres à suivre des programmes de planification familiale, davantage d'éducation sur les méthodes contraceptives ou encore des campagnes d'adoption d'enfants orphelins au lieu de la procréation à tout prix pourraient être utilisées.


### 2. Cours 2 sur la dynamique de groupe.

- #### Avant le cours

Pour ce cours sur la dynamique de groupe, le professeur nous avait demandé d'apporter un objet qui nous fait penser à une problématique qui nous tient à cœur. J'ai apporté un bol vide. Pourquoi un bol vide ? Parce que pour moi, ce bol vide représente la famine dans le monde, la problématique qui me touche le plus sur cette terre. Bien sûr, d'autres combats comme l'écologie me tiennent à cœur, mais pour moi, s'il y a un combat qui passe avant tout, c'est la famine. Pour pouvoir se lever et lutter pour rendre le monde meilleur, il faut de la force et de l'énergie. Et pour avoir cette énergie, il faut se nourrir, avoir le ventre plein.

- #### Etape 1

En arrivant en cours, la première chose que nous avons faite, c'est de poser nos objets sur le sol et de dire aux autres de quoi il s'agit. Et là, je me suis rendu compte que personne n'avait apporté quelque chose qui puisse faire penser à la famine.

##### # j'étais le seul à penser à la famine.
- #### Etape 2 

Après avoir présenté notre objet aux autres, je devais m'associer avec des personnes pour qui leur objet pouvait être lié au mien, des gens qui ont la même problematique que moi. Et là, c'est le néant. Après avoir discuté avec quelques personnes, je me suis rendu compte que personne n'avait pensé à la même chose que moi. Mais bon, j'ai fini par former un groupe avec 3 personnes, même si nos objets n'ont rien en commun. Il s'agit de [Léon Rubbens](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/leon.rubbens/) qui a apporté l'image d'un cœur de pigeon avec une pompe, [Morgan Tonglet](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/morgan.tonglet/) qui a apporté quelque chose lié à l'écologie, [Julien Calabro](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/julien.calabro/) un dé d'addiction.


- #### Etape 3 

Maintenant que nous avons formé un groupe, les coordinateurs nous ont demandé de trouver des liens entre nos différents objets. Nous avons laissé libre cours à nos cerveaux et voilà le résultat ![](images/imgMod5_1.jpg)  Comme vous pouvez le voir, le mot "Santé" vient en dernier et fait un peu le lien entre toutes nos idées. Après avoir lié nos idées, il nous a été demandé de trouver un thème général qui s'en dégage. Dans notre cas, cela avait déjà été fait. Nous avons décidé de prendre comme thème la santé.


- #### Etape 4

Maintenant que nous avons un thème, il nous a été demandé de trouver une problématique. Pour ce faire, nous devions nous adresser au groupe en commençant nos phrases par "Moi à ta place..." et un autre membre du groupe peut rebondir sur le même type en commençant lui aussi sa phrase par "Moi à ta place...". En le faisant, nous avons établi une liste, la voici : ![$](images/imgMod5_2.jpg)

Cet exercice a été particulièrement difficile au début car, comme vous pouvez le voir sur la photo, nous avons eu tendance à aller directement vers des solutions plutôt que de réfléchir aux problèmatique.


### 3. Cours 3: fonctionnement d'un groupe et prise de décision.

- #### A) La formaion 

Pour ce deuxième cours sur le dynamisme de groupe, nous avons reçu une petite formation sur la prise de décision en groupe. Sur celle-ci, trois idées ou approches pour prendre une décision seront développées ici. Évidemment, nous n'avons pas appris que cela, mais notre professeur nous a demandé d'en développer que trois

- ##### L'Art de décider

![l'art de décider](images/modul5_triangle.jpg)

Comme vous pouvez le voir sur la photo, il s'agit d'un triangle dont les sommets représentent une manière de prendre une décision.

1) ***Seul***: Quand il s'agit d'une décision légère qui n'a pas d'impact sur le projet en lui-même, comme par exemple l'endroit où le groupe va se regrouper pendant la semaine, ou quand une personne du groupe est plus compétente dans un domaine et peut prendre cette décision seule, le groupe s'en remet à cette personne. Ainsi, on n'accorde pas beaucoup de temps à discuter de cette décision. Attention à ce que toutes les décisions ne soient pas prises par une seule personne.


2) ***Collectif***: Il s'agit de décisions qui doivent être prises en groupe. Elles ont une grande importance dans la réalisation du projet. Dans ce cas, c'est le groupe tout entier qui prend la décision ensemble. De ce fait il faut se limiter aux désicions importantes et pas que toutes les décisions soient prises en groupe.

3) ***Pas de decision*** Dans la mesure où toutes les informations nécessaires pour prendre une décision ne sont pas encore disponibles, le groupe a tendance à tourner en rond et à perdre beaucoup de temps. Ne pas prendre de décision permet de reporter celle-ci à un moment où les informations nécessaires pour prendre rapidement cette décision seront disponibles. 

- ##### Méthode de décision 

il s'agit ici de comment prendre la décision. Pour celui-ci, plusieurs options nous ont été presentées. En voici quelques unes

1) ***La temperature check***

Il s'agit de soumettre un sujet à un vote, mais chaque personne a trois possibilités : soit elle lève les deux mains en l'air, ce qui signifie qu'elle est favorable au sujet, soit elle secoue les mains devant elle pour indiquer qu'elle n'est pas totalement pour mais pas contre non plus, ou elle baisse les mains vers le bas pour dire qu'elle n'est pas intéressée par le sujet.

2) ***Le consensus***

Tous les membres du groupe sont d'accord avec cette decision.

- ##### Check in / check out

Il s'agit ici de comprendre l'état d'esprit du groupe afin de savoir s'il est préférable de discuter des décisions importantes lors de la réunion ou non. Cette méthode permet également de comprendre les décisions des autres, leur état d'esprit au moment où ils ont pris la décision.

Comment procède-t-on ?
***le check in***
Avant de commencer une réunion, chaque membre du groupe exprime un peu comment il se sent en arrivant à cette réunion. Par exemple, "j'ai passé une nuit compliquée et je suis complètement épuisé" ou "je suis motivé et en forme pour bosser". Cela permet de répartir les rôles lors de la réunion, les personnes motivées pouvant animer la réunion ou transcrire les points importants tandis que les autres pourront jouer des rôles moins actifs.

Savoir dans quel état d'esprit se trouvent les autres personnes pendant la prise d'une décision peut permettre de comprendre pourquoi telle personne a tel avis.

***le check out***

Il est important de terminer la réunion en faisant un tour de table pour savoir si chacun est satisfait de la manière dont la réunion s'est déroulée et des décisions prises. Cela permet également de recueillir des commentaires ou des suggestions pour améliorer les réunions à l'avenir. Si quelqu'un n'est pas satisfait, il est important d'écouter ses préoccupations et de travailler ensemble pour trouver une solution. En fin de compte, il est essentiel que chaque membre se sente entendu et respecté pour que le groupe puisse continuer à fonctionner de manière efficace.


- #### B) Exercice


Dans ce cours, nous avons également discuté de la mise en place du fonctionnement futur de notre groupe. Nous avons établi nos méthodes de prise de décision et nos futurs rôles. Pour la méthode de décision, nous avons opté pour le consensus, afin de toujours chercher à satisfaire tout le monde, et si cela ne fonctionne pas, nous utiliserons un vote pondéré. Nous avons également décidé de séparer nos méthodes de prise de décision en deux catégories distinctes: les décisions stratégiques et les décisions opérationnelles. Pour les décisions stratégiques, nous chercherons d'abord un consensus, puis un jugement majoritaire, suivi d'un vote pondéré si nécessaire. En revanche, pour les décisions opérationnelles, nous utiliserons un vote, et en cas d'égalité, nous procéderons à un tirage au sort pour gagner du temps. Pour la répartition des rôles lors de nos réunions, nous avons utilisé une échelle de 0 à 5 avec nos mains aujourd'hui. Pour les réunions à venir, nous opterons pour une rotation des rôles et compterons sur la bonne volonté de chacun de nos membres. Si nécessaire, nous pourrons toujours revenir à notre première méthode.

### 4. Cours 4: trouver une problematique.

Ce cours fait suite au premier cours sur le dynamisme de groupe. Son but était de nous donner certaines clés pour trouver la problématique sur laquelle nous allons réaliser notre projet FabZero.

Après un bref entretien, le groupe n'avait toujours pas trouvé de problématique. L'un des coordinateurs présents nous a alors proposé une méthode : "la pyramide inverse" sur la base d'un exemple. Comment ça marche ? On part d'un thème général / un domaine de travail, ici la santé dans notre cas. Étape 1 : quelle est la direction qu'on souhaite prendre, par exemple la nutrition, l'amélioration du bien-être humain ou thérapeutique ? Dans l'exemple, nous avons choisi thérapeutique. Ensuite, on se resserre sur un système spécifique, soit physiologique, soit physique, c'est-à-dire soit une intervention sur la physiologie permettant d'aider les patients, soit un système mécanique (un outil du médecin qui peut être amélioré). Puis on converge vers une zone, par exemple le système digestif. Ainsi de suite jusqu'à trouver une problématique (qui est une question qu'on se pose).

Avec cette méthode, nous n'avons pas pu trouver de problématique dans le peu de temps qui nous était accordé. Le professeur nous a alors représenté la méthode de l'arbre à problématique et l'arbre à objectif. Mais cette fois-ci, il y avait un plus. Nous devions nous mettre des contraintes fortes pour avancer dans la prise de décision. Nous nous sommes d'abord imposé comme contrainte de travailler sur une articulation. Ensuite, nous nous sommes imposé une partie du corps : les bras, plus particulièrement les mains. Ensuite, nous avons cherché les problématiques liées à l'articulation des mains. Trois en sont sorties :

- La perte de force.
- Douleur articulaire.
- Problème de coordination.

Nous nous sommes mis une dernière contrainte, celle de travailler sur des problèmes liés aux personnes âgées. Arrivés ici, nous n'avons même pas eu besoin de choisir l'une de ces problématiques car cela nous a fait penser aux tremblements de mains chez les personnes âgées. 
Et sur cette base, nous avons construit notre arbre à problèmes et objectifs.
![l'arbre](images/Arbre.jpg)
 



 



