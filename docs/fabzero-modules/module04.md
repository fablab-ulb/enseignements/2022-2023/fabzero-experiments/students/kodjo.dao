# 4. MicroControleur.
Dans ce module je vais vous parlez de ma formation sur les microcontroleurs.

##### Qu'est ce qu'un microcontroleur (MCU en anglais)?

Un microcontrôleur est un circuit intégré compact qui effectue des opérations spécifiques au sein d'un système intégré. Si cette phrase n'est pas claire pour vous, vous pouvez consulter ce [site](https://www.techno-science.net/definition/6737.html) pour en apprendre davantage. L'explication y est bien détaillée.

#### 1. La fomation
Pour cette formation, dans le but d'apprendre et de maîtriser au minimum nos microcontrôleurs, nous avons été chargés de réaliser trois petits exercices.

Le premier exercice consiste à mettre en fonctionnement le capteur de température et d'humidité relative (DTH20) intégré au microcontrôleur.

Le deuxième exercice consiste à contrôler la LED (la LED RVB) pour créer différentes sources lumineuses.

Le troisième exercice consiste à choisir un capteur de notre choix et réussir à le faire fonctionner.

Afin de réaliser ces exercices, nous avons été présentés à deux langages de programmation (Micropython et Arduino), ainsi qu'à trois bibliothèques et exemples de code mis à notre disposition.
- **Bibliothèque MicroPython**

- **Exemples C pour les appareils I2C**

- **Bibliothèque Arduino**

##### Installation de Rasberry pi


Pour pouvoir réaliser l'exercice, la première étape consistait à installer Thonny si vous souhaitiez utiliser Micropython, ou Arduino si vous préférez le langage Arduino (qui est une version simplifiée de C++).

Personnellement, j'avais déjà Thonny car j'avais programmé dessus lors de ma deuxième année. J'ai donc choisi d'utiliser Thonny avec Micropython .

Voici un lien pour télécharger Thonny au cas où vous ne l'auriez pas, mais que vous souhaitez également utiliser Micropython [lien](https://thonny.org/).

Comme d'habitude, ce qui fonctionne pour les autres ne fonctionne pas toujours pour moi. Pour votre plaisir, je vais partager les problèmes auxquels j'ai été confronté et vous aider à les résoudre, ou du moins vous présenter comment je les ai résolus au cas où vous rencontriez les mêmes difficultés.


- ##### Etape 1: connécté le microcontroleur sur l'ordinateur.

Pour effectuer des modifications sur le microcontrôleur et enregistrer des fichiers à l'intérieur, vous devez maintenir le bouton **Boot** enfoncé avant de connecter le microcontrôleur à votre ordinateur. Sinon, vous ne pourrez utiliser que les fonctionnalités pour lesquelles il a été programmé.

- ##### Etape 2. installer Rasberry pi

Pour avoir accées au microcontroleur sur votre ordinatuer vous devez l'installer. 
voici un [lien](https://fablab-ulb.gitlab.io/enseignements/fabzero/electronics/languages/python/) qui indique comment faire.

Mais évidemment, cette méthode n'a pas fonctionné pour moi. Comme tu peux le voir sur la photo, j'obtiens une erreur quand j'essaie de télécharger Rasberry Pi.![error Rasberry pi](images/Mod_4_install_Rb.jpg)

Après cette tentative infructueuse, j'ai décidé d'utiliser Bash pour vérifier ma version et exécuter Thonny à partir de Bash.![ouverture thonny](images/Open_thonny.jpg) À ma grande surprise, on peut ouvrir Thonny à partir de Bash. J'ai réessayé de télécharger Rasberry Pi, mais ça ne fonctionne toujours pas mais j'ai quand même fait un petit pas. ![Deuxieme tenetative d'installation](images/Mod_4_RB_Bash.jpg) 
Même si j'ai toujours une erreur, cette fois-ci, j'ai un lien qui renvoie sur un site où je peux le télécharger. Je te le glisse [ici](https://micropython.org/download/rp2-pico/). 


Sur se site j'ai telécharger la dernier version. ( Attention il y'a beaucoup de Microcontroleur different. Telecharger que le fichier correspondant au microcontroleur que vous avez).
Sur ce site, j'ai téléchargé la dernière version (attention, il y a beaucoup de microcontrôleurs différents, téléchargez uniquement le fichier correspondant à celui que vous avez).

Après avoir téléchargé le fichier, je le glisse dans le dossier du microcontrôleur à chaque fois que je le connecte. À ce moment-là, j'ai accès au microcontrôleur via Thonny. Cependant, s'il ne s'affiche pas dans Thonny, fermez l'application et rouvrez-la. 

#### 2. Les exercices

- ##### Exercie 1. Activer le capeteur a temperature et humidité.

Faire cet exercice n'était pas compliqué car un exemple de code à utiliser nous a été fourni. [lien fichier](https://github.com/flrrth/pico-dht20). J'ai simplement copié le code et l'ai mis dans le dossier principal du microcontrôleur. Ce n'était pas un code particulièrement compliqué et faisait appel à des bibliothèques que je ne connaissais pas, alors j'ai préféré le copier-coller que je comprenais très bien. J'ai également copié le code de la bibliothèque "dht20" que j'ai sauvegardé sur le microcontrôleur. Il est important que le fichier porte le nom "dht20". Comme ici 

![](images/MIc3.jpg) 


Après avoir compris le code, il fallait faire le montage. Après quelques petites recherches sur internet et une vidéo YouTube, j'ai réussi à faire le montage qui n'était pas du tout compliqué, et qui était même similaire, voire identique, à celui donné en bas de l'exemple fourni par le coordinateur. Voici mon montage![photo montage capteur TH](images/Modul4_montage_th.jpg) 
Maintenant, il ne reste plus qu'à exécuter le code pour voir si ça fonctionne. Et super, ça marche, j'ai la température et l'humidité. ![code capteur de temperature et d'humidité](images/MIc2.jpg)

Évidemment, comme tous les êtres humains, j'ai le test qui s'impose à ce moment-là pour voir si elle fonctionne bien. J'ai soufflé sur le capteur pour voir si la température et l'humidité augmentaient.

- ##### Exercice 2: Adresser la LED RVB

Pour ce deuxieme exercice nous devions reussir à dresser la LED RVB qui ce trouve le microcontoleur et produire different gamme de couleur comme souhaite.
Une exemple nous également été fornis mais ce n'est pas elle que j'ai utilisé. En tapans micropython neopixele ( sur la recommandation d'un amis) je uis tombe sur ce magnifique [site](https://docs.micropython.org/en/latest/esp8266/tutorial/neopixel.html) qui donne de tres bonnes  exemple que j'ai adapter pour controler ma LED. La LED ce trouve déja sur le microcontroleur donc pas de montage à faire.

- ##### Exercice 3: Faire fonctionner un capteur

Pour cet exercice, j'ai choisi comme capteur le module microphone avec amplificateur réglable (MAX4466MICMOD). Après plusieurs échecs, avec l'aide de ChatGPT, j'ai réussi à écrire un code qui fonctionne.![code capteur microphone](images/MIc3_Microphone.jpg)
Ma difficulté était que je savais que je devais effectuer une conversion analogique vers le numérique pour la fréquence, mais je ne me rappelais plus quelles étaient les entrées analogiques de mon microcontrôleur. Voila [le Mappage](https://fablab-ulb.gitlab.io/enseignements/fabzero/electronics/hardware/specifications/)des broches de mon microcontrôleur. Si vous avez le même, les entrées/sorties sont le GP26, 27 et 28, il me semble. Vous pouvez également demander à ChatGPT en cas de doute.

Maintenant que je connais les broches analogiques, le montage était simple. ![photo montage microphone](images/module4_montage_MIcrophone.jpg)

La réponse que j'ai reçue était correcte, mais étant donné que ce sont les valeurs de crête à crête (l'amplitude du signal) qui sont données et qu'il y a du bruit autour de moi, je ne voyais pas de grandes différences entre les valeurs quand j'applaudissais pres du microphone ou non. J'ai donc ajouté du code pour allumer la petite LED GPIO25 quand la valeur dépasse un certain seuil (33300) et l'éteindre en dessous de cette valeur. Et alors j'ai remarqué que lorsque j'applaudissais près du microphone, la LED restait beaucoup plus allumée que lorsque je n'applaudissais pas. Évidemment, la lampe s'allume même lorsque je n'applaudis pas, mais à une fréquence plus lente, et cela est dû au bruit autour de moi.

Voila le code finale.
~~~
import machine
import utime 

# Configure la broche d'entrée comme entrée analogique
microphone_pin = machine.ADC(machine.Pin(26))

led_GPIO25 = machine.Pin(25, machine.Pin.OUT)


# Boucle infinie pour mesurer la tension du microphone
while True:
    # Lit la tension du microphone
    microphone_voltage = microphone_pin.read_u16()
    
    # Affiche la tension sur la console
    print(" microphone value: ", microphone_voltage)
    
    # pour voir une difference etre mes valeur 
    if microphone_voltage > 33300:
        #Allumer la petite LED GiPO25 
        led_GPIO25.value(1)
    else:
        #Éteindre la petite LED GIPO 25
        led_GPIO25.value(0)
        
    #Attendre une courte durée avant de continuer
    utime.sleep_ms(10)
~~~


Voilà, c'est la fin du module. Évidemment, je ne suis pas devenu le meilleur en électronique juste avec deux cours et quelques exercices, mais cette formation m'a permis de me rendre compte du potentiel des objets électroniques qui m'entourent et m'a fait un peu découvrir le monde de l'électronique.

Avant, je trouvais que les codes qu'on apprenait à l'école ne servaient pas à grand-chose, mais avec cette formation, j'ai pu exploiter mes connaissances en Python aprise en deuxième et utiliser ces connaissances dans du codage pour vraiment faire quelque chose. 














