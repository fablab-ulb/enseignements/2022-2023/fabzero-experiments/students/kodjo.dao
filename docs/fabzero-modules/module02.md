# 2. Conception Assistée par Ordinateur (CAO)

Pour ce module, il nous a été demandé de concevoir un objet que nous pourrons ensuite imprimer sur une imprimante 3D. À cette fin, les coordinateurs nous ont présenté trois logiciels (FreeCAD, OpenCAD, Inkscape) qui permettent de réaliser une modélisation assistée par ordinateur. Il nous a également été demandé de trouver un code permettant de construire un objet pouvant être associé au nôtre, provenant du site d'un camarade actuel ou d'éditions précédentes de Fablabzero Expérimente.

Plutôt que de simplement chercher le code chez un camarade, mes amis ([Sanou Kamel](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/kamel.sanou/), [Sami El Hamdou](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/sami.el.hamdou/), [Mariam Mekray](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/mariam.mekray/)) et moi avons décidé de fabriquer une catapulte en commun. Chaque personne devait construire une partie de la catapulte, ce qui nous a conduit à utiliser du code provenant d'un autre étudiant pour répondre à cette exigence.

### Aprntissage de FreeCAD 
Avant de commencer à créer l'objet que j'allais imprimer pour la catapulte, j'ai d'abord commencé par faire un essai. Mon essai consistait à réaliser deux trèfles qui peuvent s'assembler d'une manière unique, donc avec zero degré de liberté.

- #### Etape 1: faire un sketch 

Après avoir visionné plusieurs vidéos tutorielles sur la documentation du fablabzero experiment, dont je vous partage le [lien]( https://gitlab.com/fablab-ulb/enseignements/fabzero/cad), j'ai suivi les instructions pour créer un corps et un sketch.![ photo de](images/feesCAD_img_1.png)

Pour cela j'ai commencé à créer des arcs de cercle que j'ai reliés par des contraintes. ![capture image2](images/feesCAD_img_2.png)

- #### Etape 2: contraindre entirement sont "sketch"
Après avoir relié les points des arcs, j'ai fermé le "Sketcher", puis je suis passé à "Part Design" et j'ai effectué une "protrusion". Cependant, "la protrusion" présentait un problème. Il y a une plaque qui recouvre mon image comme vous pouvez le voir sur l'image ci-dessous.

![capture image3](images/feesCAD_img_3.png) 

Je suis retourné dans mon "Sketcher" et j'ai remarqué que je n'avais pas appliqué toutes les contraintes nécessaires à mon dessin. J'ai donc ajouté les contraintes manquantes jusqu'à ce que tout mon dessin devienne vert, ce qui signifie qu'il est entièrement contraint. Et là, quand je passe en 3D, pas de problème. 

![image5](images/feesCAD_img_5.png) 

- #### Etape 3: Paramêtré les longueurs

Apres cette l'étape 2 j'ai remarqué que je n'ai pas défini mes distances en tant que variables pour pouvoir les modifier facilement, comme le professeur nous l'avait demandé. Pour remédier à cela, je suis retourné sur mon sketch et j'ai ajouté des contraintes avec des distances variables dans un "spreadsheet". Veuillez noter que vous devez fermer la partie sketcher avant de pouvoir ouvrir le spreadsheet.

![image4](images/feesCAD_img_4.png)

Ensuite, après avoir placé les distances dans un tableur, j'ai essayé de donner différentes valeurs à mes contraintes de distance.En effet les valeurs de base de FreeCAD sont en millimètres, et si j'imprimais les trèfles avec ces valeurs, ils seraient très petits. Cependant, en modifiant les valeurs dans le tableur pour augmenté la taille, j'ai constaté que mon dessin avait complètement changé et ne ressemblait plus à un trèfle. De plus, j'ai reçu un message indiquant "le serveur n'a pas pu converger".( cf : image suisvant)

![freeCAD image](images/feesCAD_img_6.png)

Alors, il a fallu retravailler une fois de plus sur les contraintes. J'ai remarqué que j'avais fixé certains points qui ne peuvent pas bouger, ce qui empêche l'ajustement de l'échelle du sketch. Il est donc préférable d'éviter d'appliquer des contraintes qui fixent un point si vous souhaitez rendre certaines dimensions variables.
- #### Etape 4 : metre des motif 

Arrivé à ce stade, je n'avais qu'un seul trèfle. J'ai tenté de le cloner dans le même fichier pour obtenir deux objets similaires mais avec des motifs différents. Cependant, j'ai rencontré un problème : chaque fois que je modifiais l'un des clones, les modifications étaient appliquées automatiquement à l'autre. J'ai donc décidé de sauvegarder une copie de l'objet dans un dossier sous un autre nom, puis de modifier chaque trèfle séparément. Cependant, j'ai commis l'erreur de faire cela après avoir réalisé les motifs. (J'explique cette erreur plus bas dans ce module).

En m'inspirant d'un objet que le professeur nous a montré en classe, j'ai décidé de créer trois motifs en protrusion (un cercle, un triangle et un carré) sur une face d'un des trèfles, afin de pouvoir les insérer dans les cavités correspondantes sur le deuxième trèfle.

Pour cela, j'ai cliqué sur la face où je voulais créer le sketch, puis j'ai créé le sketch. Comme à l'étape 2, j'ai entièrement contraint mon sketch, puis j'ai pu créer le corps en utilisant une protrusion pour les motifs. C'est à ce moment-là que j'ai enregistré une copie du fichier avant de réaliser des cavités à la place des protrusions dans le fichier original.![potruision](images/modul2_1.jpg) ![cavité](images/modul2_2.jpg).


Suivant les mêmes étapes, j'ai décidé de créer une cavité et un trou dans le trèfle. Dans la cavité, j'ai prévu de placer un aimant qui maintiendra les deux trèfles ensemble. Et le trou me permettra d'insérer les trèfles sur un porte-clés.

Voici le fichier [Trêfle](files/version%20finale%20trefle_Potrusion.FCStd). Elle est sous licence CC BY 4.0



### Aprendre openCAD 

N'étant pas un amateur de codage, OpenSCAD n'est pas le logiciel que j'ai le plus utilisé. Cependant, j'ai décidé de regarder les petits [tutoriels](https://gitlab.com/fablab-ulb/enseignements/fabzero/cad/-/blob/main/OpenSCAD.md) de notre coordinateur de notre coordianteur Nicolas De Coster, pour apprendre à l'utiliser. J'ai suivi la vidéo pour réaliser certains des exemples.

### Concepetion en groupe 

Comme mentionné dans l'introduction, notre groupe d'amis a entrepris la création d'une catapulte. Voici une image de [la catapulte](images/la_catapulte.jpg). Cependant, au cours de sa réalisation, nous avons décidé d'apporter quelques modifications. Ma partie du projet consistait à créer un parallélépipède et deux cylindres : le premier, représenté en violet sur le dessin, qui sert d'arrêt pour la cuillère, et un autre cylindre qui passe entre le petit parallélépipède fixé à la base et la cuillère. En plus de ces deux cylindres.


- #### Les deux cylindres. 

Malgré mon manque de passion pour la programmation, j'ai décidé d'utiliser OpenSCAD pour créer l'un de mes objets. Bien sûr, j'ai opté pour les plus simple : les deux cylindres.

Il m'a suffi de deux lignes de code pour créer un cylindre. Étant donné que je peux facilement modifier les paramètres de mon cylindre, j'ai décidé d'en créer un seul et d'ajuster le rayon et la hauteur pour obtenir deux versions différentes à imprimer.

![la cylindre](images/module2cylindres.jpg)

- ####  Le parallélepipède fixé à la base.

Pour le parallélépipède fixé à la base, je suis retourné sur FreeCAD. J'ai créé un simple sketch en utilisant des polylignes et un cercle. J'ai ajouté des contraintes jusqu'à ce que tout soit entièrement contraint, comme le montre l'image ci-dessous :

![](images/Mod2_PAR1.jpg)

J'ai ensuite mis toutes les dimensions dans un spreadsheet et créé une protrusion.

Ensuite, j'ai réalisé un rectangle sur l'une des faces, ce qui m'a permis de créer une cavité dans l'ensemble, comme illustré ci-dessous.![](images/Mod2Para2.jpg)


À ce stade, j'ai remarqué que j'avais un petit problème. Les bords avant et arrière dépassaient et pouvaient reposer sur la base, mais ce n'était pas le cas pour les côtés gauche et droit. Si je voulais respecter la taille du trou dans la base de la construction de Sami, dans laquelle je vais insérer ce parallélépipède, je ne pouvais pas augmenter l'épaisseur de ma construction.

J'ai donc recréé des sketchs comme indiqué sur l'image, en dessinant des rectangles superposés sur les côtés de mon ancien sketch pour augmenter l'épaisseur. Attention, même si cela ne se voit pas sur l'image, les points du rectangle que je viens de créer sont fixés sur ceux de l'ancien sketch. Pour faire cela, j'ai utilisé l'option "géométrie externe" et j'ai cliqué sur les bords de mon parallélépipède pour faire apparaître les cotes, comme illustré ci-dessous. J'ai ensuite pu fixer les points correctement.
![](images/Mod2Para4.jpg)

Après avoir créé la protrusion à partir de ce nouveau sketch, celle-ci recouvrait le trou que j'avais précédemment créé. J'ai donc créé un nouveau trou.

Pour m'assurer que ce nouveau trou soit au même emplacement que l'autre, j'ai réaffiché la "géométrie externe" des cotes précédentes. J'ai tracé des diagonales que j'ai reliées aux points des anciennes cotes du parallélépipède. Ensuite, j'ai créé un point sur l'une des diagonales et l'ai contraint à appartenir également à la deuxième diagonale. Ainsi, j'ai obtenu le milieu du rectangle et j'ai créé le schéma de mon cercle à cet endroit. C'est exactement ce que j'avais fait pour créer ma première cavité. Pour que les diagonales que j'ai créées n'apparaissent pas sur ma figure, je les ai mises en mode "géométrie de construction", ce qui explique leur couleur bleue.

Enfin, pour faciliter l'entrée du cylindre, j'ai créé une petite chambre sur les bords du cercle. J'ai également créé des chambres sur les bords intérieurs de toute la construction, mais cela était uniquement pour un aspect esthétique.

#### Licence Creative commun


Le professeur nous avait également demandé de mettre une licence Creative Commons sur nos créations afin de permettre à d'autres personnes de les utiliser. Pour cela, je suis allé sur [site](https://creativecommons.org/about/cclicenses/) pour copier le lien d'une licence Creative Commons que j'ai collé dans la case dédiée à cela sur FreeCAD. J'ai indiqué mon nom et le nom du projet.

Je n'ai pas ajouté de licence pour le cylindre, car le code est très simple et je ne le juge pas nécessaire.


voila le fichier de création du [parallélepipède](files/parall%C3%A9lopip%C3%A8de.FCStd)

Ce ficher,Parallélépipède rectangle du Catapulte est sous licence CC BY 4.0


#### Erreur de plan de ma trefles.

Après avoir importé mon fichier pour l'impression de la trèfle, j'ai réalisé une terrible erreur. Comme vous pouvez le voir ci-dessous, les deux trèfles ne peuvent pas s'emboîter correctement. En effet, étant donné que l'un est pratiquement une copie de l'autre, j'ai oublié de prendre en compte l'inversion des plans. Normalement, je devrais inverser la position du triangle et du carré sur l'un des trèfles. En raison de cette erreur, j'ai décidé de n'imprimer qu'une seule des trèfles et d'imprimer l'autre ultérieurement.

![](images/Prusa3.jpg)
 





 






