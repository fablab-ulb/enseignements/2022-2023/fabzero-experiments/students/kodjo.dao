# 1.  Documentation. 

Après une présentation générale, la formation a commencé par un petit cours sur la création de notre propre site web. Un site sur lequel tout notre parcours sera documenté. Vous êtes actuellement sur mon site. 

je vous souhaite une bonne lecture, j'espère que vous trouverez ce dont vous avez besoin. Car en effet cette documentation à non seulement pour objectif de  retracer l'ensemble de mon parcours dans cette formation au FabZero expérimental, en abordant les problèmes auxquels j'ai dû faire face et comment je les ai résolus, mais également de proposer des solutions à toute personne qui la consultera, notamment si elle rencontre les mêmes problèmes.
 

####  #  Même pas encore commencer et déjà un premier  problème.

Avant même de commencer ce cours, il était nécessaire de télécharger et de se connecter à GitLab. Et à ce stade, j'ai rencontré un premier problème : mon inscription ne fonctionnait pas. 

J'ai essayé plusieurs fois avec mon adresse e-mail de l'ULB, mais cela n'a pas fonctionné. Finalement, j'ai essayé avec mon adresse e-mail personnelle et un autre identifiant que mon nom, et cela a fonctionné.
Maintenant, je peux me lancer dans la formation elle-même.

## Git
### 1.1 Qu'est-ce que git.

Git est un logiciel de contrôle de version créé en 2005. C'est un outil qui permet de conserver un historique des modifications effectuées sur un projet, afin de pouvoir rapidement identifier les changements effectués et de revenir à une ancienne version en cas de problème. En plus d'être open source, ce logiciel présente de nombreux avantages et facilite la collaboration entre plusieurs personnes travaillant sur un même projet. Pour en savoir plus, vous pouvez consulter des ressources en ligne, telles que [ce site](https://www.pierre-giraud.com/git-github-apprendre-cours/presentation-git-github/).

Vous comprenez donc que ce logiciel est utile non seulement pour la documentation de cette formation, mais aussi pour faciliter la collaboration en groupe sur notre projet final.



### 1.2 installation et utilisation de Git.


Si vous possédez déjà Git Bash ou un terminal (si vous utilisez un Mac), vous pouvez passer à l'étape suivante. Pour vérifier, vous pouvez utiliser la commande suivante :
~~~
git --version 
~~~

Cette commande vous permettra de vérifier si Git est déjà installé sur votre système et vous donnera la version actuellement installée.

Si Git n'est pas installé, vous pouvez le télécharger et l'installer à partir de [ce site](https://git-scm.com/). 

- #### la configuration de git 

Sur Git Bash, vous pouvez utiliser les commandes suivantes pour configurer votre Git. Veillez à utiliser les mêmes identifiants et adresses e-mail que ceux que vous utilisez pour vous connecter à GitLab.
~~~ 
git config --global user.name " votre_identifiant"
git config --global user.email "votre_mail"
git config --global --list
// regarder bien si la list affiche les nom et le mail que vous venez de configuré
~~~

Vous pouvez remplacer "global" par "local" si vous souhaitez configurer Git dans un répertoire spécifique.

- #### faire une clé SSH 

Une clé SSH (Secure Shell) est un protocole d'identification qui permet de se connecter à des serveurs de manière sécurisée. Il existe plusieurs types de clés. Dans mon cas, j'ai utilisé la clé de type "ed25519".
~~~
ssh-keygen -T ed25519 -c "le_mail_de_git"
// L'exécutable vous propose un chemin où sauvegarder la clé. Si cela vous convient, vous pouvez l'accepter.
~~~

Ensuite, vous pouvez saisir le mot de passe ou la passphrase que vous souhaitez (veillez à bien conserver ce mot de passe, vous en aurez besoin chaque fois que vous ajouterez une modification à votre site).

PS: Même si le mot de passe ne s'affiche pas, il est bien enregistré (c'est fait exprès).

Après avoir généré la clé SSH, vous pouvez créer une copie de la clé publique que vous allez ajouter dans votre GitLab. Pour cela, vous pouvez utiliser la commande suivante:
~~~
cat ~/.ssh/ed25519.pub | clip.
~~~
Pour les personnes utilisant un système autre que Windows, la commande peut être différente. Rendez-vous sur [le site](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-via-ssh) pour savoir quelle commande utiliser. Si vous exécutez la commande et que la clé ne vous est pas présentée, pas de souci. Vous pouvez vous rendre dans votre répertoire et suivre le chemin où la clé a été sauvegardée. Après l'avoir trouvez vous pouvez l'ouvrir avec un éditeur de texte, la copier et la coller dans le champ "Key" se trouvant dans l'onglet "SSH Keys" de votre compte GitLab.

- #### Cloner les dossiers.


À présent, vous pouvez cloner votre site pour commencer à y apporter des modifications. Pour ce faire, cliquez sur "Clone" dans votre GitLab et copiez le lien associé à la clé SSH. Ensuite, collez-le dans votre terminal de commande et exécutez la commande suivante:
~~~
git clone // + le lien
~~~

- #### Commande de base de git.

Voici quelques commandes de base à connaître pour pouvoir sauvegarder vos versions et les envoyer au serveur :

|commande | fonction
---------|-------------------------------------|
| git pull|  permet de télécharger la dernier version de vos modification|
| git add le_nom_du_ficher| permet d'ajouter les modifications qui ont été faites dans le_nom_du_ficher.|
| git add *| permet d'ajouter tous les modifications qui ont été réalisés dans les répertoires ou on se trouve. |
| git commit -m "commentaire" | permet de crée un instantané ( un commit) de votre dossier.|
| git push | envoie ce commit vers le server. |


## Mkdocs

Maintenant que vous avez cloné le dépôt de votre projet sur votre machine, vous pouvez ouvrir les dossiers et effectuer les modifications nécessaires.
- #### Les liens à remplacés 
Pour que toutes les modifications effectuées dans votre clone soient reflétées sur votre site, vous devez remplacer les deux premiers liens dans le fichier mkdocs.yml par les liens de votre site et de votre GitLab. Le premier lien correspond à celui de votre site et le deuxième à celui de votre GitLab.

Si j'ai bien compris, en faisant cela, vous pouvez créer un lien entre votre page GitLab, votre clone et votre site, ce qui permettra de synchroniser les modifications effectuées sur votre clone avec votre site.
- #### Installation de Mkdocs 

Il est probable que vous ayez besoin de MkDocs pour certaines commandes, telles que "mkdocs serve", afin de visualiser localement les modifications apportées à votre site avant de les mettre à jour sur le serveur global. Pour cela, vous devez installer le logiciel MkDocs. Pour l'installation, veuillez consulter [ce site](https://www.mkdocs.org/). 
Il se peut que vous rencontriez des problèmes, comme moi.

Tout d'abord, il est possible que lorsque vous essayez d'installer MkDocs, Bash signale que vous n'avez pas toutes les licences nécessaires. 
![ mkdocs échoué](images/instalation_echoué_de_mkdocs.png )

Dans ce cas, il vous suffit de fermer le terminal (Git Bash) et de le rouvrir en tant qu'administrateur, et le problème devrait être résolu.

Deuxièmement, si vous parvenez à télécharger MkDocs mais que le terminal ne reconnaît pas la commande "mkdocs", comme illustré sur la photo ci-dessous. Dans ce cas, vous devez précéder toutes vos commandes contenant "mkdocs" par:
~~~
 python -m  // +la commande
~~~ 
Cela devrait résoudre le problème.
  ![lien extention python](images/bash1.png ) 

- #### Installer un autre thème que celui par défaut.

Pour ce faire, vous pouvez utiliser la même commande que sur la photo (ici, "windmill-dark" est le nouveau thème).![](images/mkdocs_theme_reussi.jpg.png) Assurez-vous d'avoir correctement installé le thème en question. Ensuite, vous devez ajouter ce nouveau thème au fichier "requirements.txt". Sinon, le serveur global ne pourra pas le télécharger lors de la mise à jour globale de votre site, et aucune mise à jour ne pourra être effectuée.


## Insérer et travailler sur les  images.
- #### Insertion des images.
Insérer des images est une étape que j'ai dû réaliser et que vous devrez également effectuer.

Pour cela, des répertoires sont déjà disponibles dans votre espace de travail, que vous avez cloné sur votre ordinateur.

Veillez à ce que les images se trouvent au même emplacement que le module dans lequel vous souhaitez les insérer.


- #### Travailler sur les images.

Il existe plusieurs logiciels qui permettent de travailler sur des images, tels que GraphicsMagick ou ImageMagick. Avec ces logiciels, vous pouvez modifier la taille des images ou ajouter des effets, par exemple.

Dans mon cas, j'ai choisi d'utiliser GraphicsMagick. Pour le télécharger, il vous suffit de rechercher "GraphicsMagick" sur un moteur de recherche.

Veuillez noter que cette application ne dispose pas d'interface graphique, vous devrez donc l'utiliser via le terminal Bash.

Voici un exemple de modification :![](images/gm.jpg)

Comme vous le savez déjà, la première commande me permet d'accéder au répertoire "docs", puis au sous-répertoire "images".

Ensuite la commande:
~~~
$ gm convert -resize 600x600 sample-photo.jpg sample-photo-resize.jpg
~~~
Ma permis de reduire la taille de la photo, de passé d'une taille plus grande à une petite ![](images/sample-photo.jpg) ![](images/sample-photo-resize.jpg)

Remplacez cette commande par celle qui suit et vous pouvez coller deux photo. 
~~~
gm convert -append sample-photo.jpg Avatar-photo.jpg sample-photo-resize.jpg
~~~
![](images/smple-photo_mdf.jpg).
