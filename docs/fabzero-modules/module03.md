# 3. Impression 3D

Pour pouvoir imprimer les objets que nous avons conçus à l'aide de logiciels de CAO tels qu'OpenSCAD, FreeCAD et Inkscape, il nous a été demandé de télécharger PrusaSlicer. Ce logiciel nous permet d'effectuer les réglages nécessaires avant l'impression sur l'imprimante 3D. Il génère également le code reconnu par la machine d'impression.

Lors de notre arrivée en cours, nous avons suivi une formation de deux heures sur son utilisation, puis nous avons pris en main le logiciel pour nos propres projets.

### Transfert du ficher vers PrusaSlicer.

Pour pouvoir imprimer mes objets, j'ai d'abord exporté mon fichier depuis FreeCAD, puis je l'ai ouvert dans PrusaSlicer. Pour ce faire, il suffit d'appuyer sur l'icône en forme de cube avec un signe "+" en bas, ce qui affiche votre conception sur une image du plateau d'impression, comme ceci.![$](images/Prusa1.jpg)

Ensuite, j'ai activé l'option de bordure pour ajouter une bordure et j'ai réglé le taux de remplissage de mon cylindre à 15 %. J'ai ensuite segmenté le modèle pour estimer le temps nécessaire à l'impression de l'objet. Au total, cela prendrait 2 heures et 19 minutes.

Cependant, étant donné que le cours se terminait dans moins d'une heure, j'ai dû réduire la taille de mon objet. Avec l'aide de mes camarades, nous avons décidé de nouvelles dimensions afin d'obtenir un objet plus petit, tout en restant suffisamment précis pour servir d'essai. Je suis donc retourné dans FreeCAD, où j'ai ajusté les dimensions dans le tableur, puis j'ai exporté un nouveau fichier.![$](images/Prusa2.jpg). 
Et voilà, j'ai obtenu un objet qui peut être imprimé en 5 minutes seulement.![$](images/prusa5.jpg)
 

#### Réglage necéssaire avant impression.

Comme vous pouvez le voir sur la photo ci-dessous, avant ma première impression, je n'ai pas effectué les réglages nécessaires, ce qui a résulté en une impression de mauvaise qualité, comme le montre la photo. La surface n'est pas lisse et les couches sont épaisses. ![module3_photo_cylindre](images/module3_photo_cylindre.jpg).

Un coordinateur m'a alors fait remarquer que je n'avais pas réglé le chauffage du plateau et du filament. J'ai donc revisité mon fichier dans PrusaSlicer et, en regardant les réglages de mes amis, j'ai remarqué qu'ils n'étaient pas les mêmes que les miens. J'ai donc copié leurs réglages afin d'obtenir un objet plus lisse, avec des couches beaucoup plus fines, comme on peut le voir sur la photo suivante.
![image regle](images/prusa4.jpg)

En découvrant ces réglages, j'ai également constaté qu'il était possible de choisir le type de motif de remplissage pour l'objet. Cependant, il est important de faire preuve de prudence, car le type de motif sélectionné peut influencer la durée d'impression.

De plus, la position de l'objet a également une incidence sur la durée d'impression. Par exemple, pour la taille finale de mon cylindre, l'impression prendrait 36 minutes en position verticale, tandis qu'elle ne prendrait que 13 minutes en position horizontale.
![](images/Prusa8.jpg), ![$](images/Prusa7.jpg)

#### Impresion 

Pour imprimer les conceptions, il suffit d'exporter le fichier sous forme de "G-code" sur une carte micro SD, car ce sont ces cartes qui peuvent être insérées dans l'imprimante 3D pour réaliser l'impression de l'objet. Il vous suffit ensuite d'insérer cette carte dans l'imprimante 3D, de sélectionner votre fichier et vous pouvez lancer l'impression.

Voici la machine que nous avons utilisée pour imprimer, ainsi que les objets que j'ai réalisés:
![](images/imprimate3D.jpg)
![](images/grand_cylindre.jpg)
![](images/Petit_cylindre.jpg)
![](images/Parall%C3%A9l%C3%A9pip%C3%A8de.jpg)
![](images/la%20Catapulte.jpg)




